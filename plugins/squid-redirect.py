#!/usr/bin/env python3
"""
Copyright 2019  Kasm Technologies LLC
file: squid-redirect.py
Adapted from: https://mindchasers.com/dev/app-squid-redirect
"""

import re
import sys
import os
from datetime import datetime
import urllib.parse

xxx = re.compile('\.xxx?/$')

def main():
    """
        keep looping and processing requests
        request format is based on url_rewrite_extras "%>a %>rm %un"
    """
    kasm_url = os.getenv('KASM_URL', 'https://www.kasmweb.com')
    request  = sys.stdin.readline()
    while request:
        [ch_id,url,ipaddr,method,user]=request.split()
        message = datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ': Kasm_Redirector: ' + request
        
        if url.startswith(kasm_url) or method == "CONNECT":
          response = ch_id + ' ERR'
        else:
          dest = urllib.parse.quote_plus(url)

          response  = ch_id + ' OK'
          response += ' status=302 url=' + kasm_url + '/#/go?kasm_url=' + dest
        
        response += '\n'
        sys.stdout.write(response)
        sys.stderr.write(message)
        sys.stderr.flush()
        sys.stdout.flush()
        request = sys.stdin.readline()

if __name__ == '__main__':
    main()
